let resolveExecutable;
self.executable = new Promise((resolve) => (resolveExecutable = resolve));

self.Module = {
  locateFile: self.getFileBlob,
  onRuntimeInitialized() {
    resolveExecutable({
      start: () => self.Module.ccall("start", null, [], [], { async: true }),
    });
  },
};
