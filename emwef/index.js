#!/usr/bin/env node

const { promises: fs } = require(`fs`);
const { gzip } = require(`zlib`);
const { promisify } = require(`util`);
const path = require(`path`);

const main = async () => {
  const programName = process.argv[2];
  if (programName === undefined) {
    console.error(`Program name was not specified`);
    process.exit(1);
  }

  console.log(`emwef: compiling ${programName}.wef...`);

  const entryBase = await fs.readFile(path.join(__dirname, `entryBase.js`));
  const emscriptenJs = await fs.readFile(`${programName}.js`, `utf8`);
  const mainChunk = Buffer.from(entryBase + emscriptenJs);

  const emscriptenWasm = await fs.readFile(`${programName}.wasm`);

  const metadata = {
    entry: `${programName}.js`,
    files: [
      [`${programName}.js`, mainChunk.length],
      [`${programName}.wasm`, emscriptenWasm.length],
    ],
  };
  const finalContents = [
    ...Buffer.from(JSON.stringify(metadata)),
    0,
    ...mainChunk,
    ...emscriptenWasm,
  ];

  const gzipped = await promisify(gzip)(Buffer.from(finalContents), {
    level: 1,
  });
  await fs.writeFile(`${programName}.wef`, gzipped);

  console.log(`emwef: done`);
};

main();
