const ERRORS = {
  NotFound: -2,
  AlreadyExists: -17,
  IsDirectory: -21,
  NotDirectory: -20,
  PermissionDenied: -13,
  InvalidOffset: -29,
  CorruptedExecutable: -8,
  InvalidArgument: -22,
  UnsupportedOperation: -38,
  DeviceBusy: -16,
  NotEmpty: -39,
  CrossDevice: -18,
};

const processResult = (result) => {
  if (`Err` in result) {
    return ERRORS[result.Err] ?? -(2 ** 31);
  }

  if (result.Ok === undefined) {
    return 0;
  }

  if (typeof result.Ok === `number`) {
    return result.Ok;
  }

  return -1;
};
