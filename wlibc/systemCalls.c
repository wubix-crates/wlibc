#include <emscripten/em_macros.h>
#include <emscripten/em_js.h>
#include <stddef.h>

EM_JS(int, __wubix_flush, (int fd), {
    console.log(`flush`);
    return Asyncify.handleAsync(() => self.systemCalls.flush(fd).then(processResult));
});

EM_JS(int, __wubix_write, (int fd, const void *buf, size_t count), {
    return Asyncify.handleAsync(async () => {
        const bytes = [...Module.HEAP8.subarray(buf, buf + count)];
        return self.systemCalls.write(fd, bytes).then(processResult);
    });
});
