#include <emscripten/em_macros.h>
#include <string.h>
#include "./systemCalls.c"

int EMSCRIPTEN_KEEPALIVE puts(const char *s) {
    int result;

    while (*s != '\0') {
        result = __wubix_write(1, s, strlen(s));
        if (result < 0) {
            return result;
        }
        s += result;
    }

    result = __wubix_write(1, "\n", 1);
    if (result < 0) {
        return result;
    }

    result = __wubix_flush(1);
    if (result < 0) {
        return result;
    }

    return 0;
}
