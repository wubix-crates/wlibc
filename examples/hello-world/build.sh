#!/usr/bin/env bash

emcc main.c -o hello-world.js \
    --post-js ../../wlibc/systemCalls.js \
    -s ASYNCIFY -s ASYNCIFY_IMPORTS="[__wubix_flush,__wubix_write]" \
    -s EXTRA_EXPORTED_RUNTIME_METHODS="[ccall]" \

../../emwef/index.js hello-world
