# wlibc

A PoC that one can write programs for Wubix in C.

**Disclaimer:** all the code here was written by a person who never learned to
write C code specifically and who never wrote C code for anything serious.
Therefore, the C code in this repository is terrible and is terribly compiled
(the latter is not my fault, it's the fault of an ecosystem that hasn't come up
with a usable build system for all those 50 years).

## What does it look like?

There's already an example of a Hello World program [here][example].
In [`main.c`], the file `../../wlibc/stdio.h` is included — that's the
`#include <stdio.h>` for Wubix, except that it only has `puts`. Also, [`main.c`]
defines a function `void start()`:

```c
#include "../../wlibc/stdio.c"
#include <emscripten/em_macros.h>

void EMSCRIPTEN_KEEPALIVE start() {
    puts("Hello from C!");
}
```

> `EMSCRIPTEN_KEEPALIVE` is there to ensure that this function doesn't get
> compiled away.

Now we only need to compile the program for Wubix! First, run this simple
command:

```bash
emcc main.c -o hello-world.js \
    --post-js ../../wlibc/systemCalls.js \
    -s ASYNCIFY -s ASYNCIFY_IMPORTS="[__wubix_flush,__wubix_write]" \
    -s EXTRA_EXPORTED_RUNTIME_METHODS="[ccall]" \
```

> Be sure to install emscripten v2.0.13 because newer versions are so fucked up
> they don't include `malloc` no matter how hard you try; who needs it anyway?

Once you have `hello-world.{js,wasm}` files, run `emwef` (yes, it's like
`pkg2wef`, but for emscripten):

```bash
../../emwef/index.js hello-world
```

And there you go! Just upload `hello-world.wef` to your Wubix installation and
enjoy runni`Segmentation fault (core dumped)`

## What's implemented?

`wlibc` as of now only implements `puts`, defined in `stdio.c`. There's also
`wlibc/systemCalls.c` which defines low-level `__wubix_write` and
`__wubix_flush` needed to implement `puts`.

Since this is only a PoC, nothing else is planned for implementation. If you
really want to bring C to Wubix, feel free to submit your merge requests.

## Conclusion

Use Rust.

[example]: ./examples/hello-world
[`main.c`]: ./examples/hello-world/main.c
